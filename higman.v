(* A proof of Higman's lemma for decidable relations in Coq/Ssreflect. *)
(* This is based on direct inductive characterization of almost-full
   (AF) relations.  For the characterization of AF relations, see
     D. Vytiniotis, T. Coquand, and D. Wahlstedt.
     Stop when you are almost-full - Adventures in constructive termination.
     ITP 2012, LNCS 7406, pp. 250-265.
   Its implementation in Coq is available from:
     http://research.microsoft.com/en-us/people/dimitris/af-itp2012.tgz
   In order to use it with Coq 8.4pl5 or later, you need to apply the
   patch "af-itp2012-8.4pl5.diff", which is available from
   https://bitbucket.org/mituharu/tpp2015/src/ .  *)
Add LoadPath "af-itp2012/code/prop-bounded".
Require Import AlmostFull AFConstructions.
Require Import ssreflect ssrfun ssrbool eqtype ssrnat seq.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Lemma af_reflect (X : Set) (Rp : X -> X -> Prop) Rb:
  (forall x y : X, reflect (Rp x y) (Rb x y)) ->
  almost_full Rp -> almost_full Rb.
Proof.
  move => Hr Ha.
  by apply: (af_strengthen Ha) => x y /Hr.
Qed.

Lemma af_leq : almost_full leq.
Proof.
  exact: af_reflect @leP leq_af.
  (* by apply: (af_strengthen leq_af) => x y /leP. *)
Qed.

Definition option_lift (X : Set) (A : X -> X -> Prop) (x y : option X) := 
  match x, y with 
  | Some x0, Some y0 => A x0 y0 
  | None, None  => True
  | _, _ => False 
  end.

Lemma af_option_lift (X : Set) (A : X -> X -> Prop):
  almost_full A -> almost_full (option_lift A).
Proof.
  elim => {A} [A afA | A H0 IH];
    first by apply: AF_SUP => -[x|]; apply: AF_SUP => -[y|];
             apply: AF_ZT => -[?|] /=; auto.
  apply: AF_SUP => -[x|];
    first by apply: (af_strengthen (IH x)) => -[?|] [?|]; auto.
  apply: AF_SUP => -[x|];
    first by apply: (af_strengthen (IH x)) => -[?|] [?|]; auto; case; auto.
  by apply: AF_ZT => -[?|] [?|] /=; auto.
Qed.

Section HigmansLemma.

Variable X : Set.

Fixpoint seq_embed (R : X -> X -> Prop) (s t : seq X) :=
  if s isn't x :: s' then True
  else if t isn't y :: t' then False
       else seq_embed R s t' \/ (R x y /\ seq_embed R s' t').

(* Notation "R ^*" := (seq_embed R) (at level 2, format "R ^*"). *)

Lemma seq_embed0s (R : X -> X -> Prop) (t : seq X) :
  seq_embed R [::] t.
Proof.
  by case: t.
Qed.

Hint Resolve seq_embed0s.

Lemma seq_embed_size (R : X -> X -> Prop) (s t: seq X) :
  seq_embed R s t -> size s <= size t.
Proof.
  elim: t => [| y t IHt] in s *; first by case: s.
  by case: s => [| x s] //= [/IHt | [_ /IHt]] //=; rewrite ltnS => /ltnW.
Qed.

Lemma seq_embed_true_size (s t : seq X) :
  seq_embed (fun _ _ => True) s t <-> size s <= size t.
Proof.
  split; first by exact: seq_embed_size.
  elim: t => [| y t IHt] in s *; first by case: s.
  by case: s => [| x s] //=; rewrite ltnS => /IHt; right.
Qed.

Lemma seq_embeds0 (R : X -> X -> Prop) (s : seq X) :
  seq_embed R s [::] <-> s = [::].
Proof.
  split; last by move => ->.
  by move/seq_embed_size; case: s.
Qed.

Lemma seq_embed1s (R : X -> X -> Prop) (x : X) (t : seq X) :
  seq_embed R [:: x] t <-> exists i, i < size t /\ R x (nth x t i).
Proof.
  split.
  - elim: t => [| y t IHt] //=.
    by case => [/IHt [i [Hi1 Hi2]]| [H1 H2]]; [exists i.+1 | exists 0].
  - elim: t => [[i] [] | y t IHt]; first by rewrite ltn0.
    move => [] [| i] /= []; auto.
    by rewrite ltnS => Hi1 Hi2; left; apply: IHt; exists i.
Qed.

Lemma seq_embed_cat (R : X -> X -> Prop) (s1 s2 t1 t2 : seq X) :
  seq_embed R s1 t1 -> seq_embed R s2 t2 -> seq_embed R (s1 ++ s2) (t1 ++ t2).
Proof.
  elim: t1 => [| y t1 Ht1] in s1 s2 *; first by move/seq_embeds0 => ->.
  case: s1 => [| x s1 /= [H1 | [H2 H3]] H4].
  - case: s2 => [| x s2 _ H1] //=.
    by left; exact: (Ht1 [::] (x :: s2)).
  - by left; exact: Ht1 H1 H4.
  - by right; split => //; exact: Ht1.
Qed.

Lemma seq_embed_strengthen (A B : X -> X -> Prop) (s t : seq X) :
  seq_embed A s t -> (forall x y, A x y -> B x y) -> seq_embed B s t.
Proof.
  elim: t => [| y t IHt] in s *; first by case: s => [| x s]; auto.
  by case: s => [| x s] //= [ | []]; auto.
Qed.

Lemma seq_embed_union_unary (A B: X -> X -> Prop) (x : X) (s t : seq X):
   seq_embed (fun y z => A y z \/ B x y) s t ->
   seq_embed A s t \/ seq_embed B [:: x] s.
Proof.
  elim: t => [| y t IHt] in s *; first by case: s; auto.
  case: s => [| x' s] /=; auto.
  by move => [/IHt [] | [[H1 | H2] /IHt []]]; auto.
Qed.

(* Higman's lemma restricted to the case that size <= 1 *)
Lemma af_seq_embed_take1 (R : X -> X -> Prop):
  almost_full R -> almost_full (fun s t => seq_embed R (take 1 s) (take 1 t)).
Proof.
  move => afR.
  apply: (af_strengthen (af_cofmap ohead (af_option_lift afR))) => s t.
  case: s => [| ? s]; case:t => [| ? t] //=.
  by case: s; auto.
Qed.

Section DecidableSplit.

Variable R : X -> X -> Prop.
Hypothesis decR : dec_rel R.

(* In dec_rel, orders of both args and sumbool constructors are
   flipped from the usual ones.  So we locally define a boolean
   version so as to avoid confusion.  *)
Let decRb x y : bool := ~~ decR y x.

Lemma decRbP: forall x y : X, reflect (R x y) (decRb x y).
Proof.
  move => x y.  rewrite /decRb.  by case: decR => H; constructor.
Qed.  

Lemma seq_embed1sP (x : X) (s : seq X) :
  reflect (seq_embed R [:: x] s) (has (decRb x) s).
Proof.
  apply: (iffP (has_nthP x)).
  - move => [i Hi Hd].  apply/seq_embed1s.
    exists i; split => //.  by apply/decRbP.
  - move/seq_embed1s => [i [Hi Hd]].  exists i => //.
    by apply/decRbP.
Qed.

Definition dec_split (x : X) (s : seq X) :=
  let: n := find (decRb x) s in (take n s, drop n s).

Lemma fst_dec_split (x : X) (s : seq X) :
  ~ seq_embed R [:: x] (dec_split x s).1.
Proof.
  move/seq_embed1s => [i].
  rewrite /dec_split.
  set n := find _ _.
  rewrite size_takel ?find_size //.
  case => /(fun x => (x, x)) [Hi /(before_find x)].
  by rewrite nth_take // => /decRbP.
Qed.

CoInductive snd_dec_split_spec (x : X) (s : seq X) : seq X -> Prop :=
| SndDecSplitNil of ~ seq_embed R [:: x] s :
    snd_dec_split_spec x s [::]
| SndDecSplitCons y t of seq_embed R [:: x] s & R x y :
    snd_dec_split_spec x s (y :: t).

Lemma snd_dec_splitP (x : X) (s : seq X) :
  snd_dec_split_spec x s (dec_split x s).2.
Proof.
  rewrite /dec_split /=.
  set n := find _ _.
  case: (boolP (has (decRb x) s)) => /(fun x => (x, x)) [Hh].
  - rewrite has_find -/n => Hn.
    have: size (drop n s) > 0 by rewrite size_drop subn_gt0.
    case Hd: (drop n s) => [| y t] // _.
    constructor; first by apply/seq_embed1sP.
    rewrite (_: y = nth x (drop n s) 0);
      last by move/(congr1 (nth x ^~ 0)): Hd => /= <-.
    rewrite nth_drop addn0.
    move/(nth_find x): Hh.  by apply/decRbP.
  - rewrite has_find -/n -leqNgt => /drop_oversize ->.
    constructor.  by apply/seq_embed1sP.
Qed.      

End DecidableSplit.

(* Higman's lemma for decidable relations *)
Lemma af_seq_embed_dec (R : X -> X -> Prop) (decR : dec_rel R) :
  almost_full R -> almost_full (seq_embed R).
Proof.
  move=> afR.  elim: afR => {R} [R HR | R afR' IH] in decR *.
  - apply: (af_strengthen (af_cofmap size af_leq)) => s t /seq_embed_true_size.
    by move/seq_embed_strengthen; apply.
  - apply: AF_SUP.
    elim => [| x0];
      first by apply: (af_strengthen (AF_ZT (fun _ _ => I))) => x y _; right.
    have decR': dec_rel (fun y z : X => R y z \/ R x0 y)
      by move => x y /=; case: (decR x y); case: (decR y x0); auto; left => -[].
    move/(_ _ decR') in IH.
    case => [_| x1 s1];
      first by apply: (af_strengthen IH) => x y;
      exact: seq_embed_union_unary.
    set s0 := x1::s1 => IHs0.
    pose ds := dec_split decR x0.
    have Hds1: almost_full (fun s t : seq X => seq_embed R (ds s).1 (ds t).1)
      by apply: (af_strengthen (af_cofmap (fun s => (ds s).1) IH));
         move => s t /seq_embed_union_unary [] // /fst_dec_split.
    have Hds2t := af_cofmap (fun s => (ds s).2)
                            (af_seq_embed_take1 (AF_SUP afR')).
    have Hds2d := af_cofmap (fun s => drop 1 (ds s).2) IHs0.
    apply: (af_strengthen (af_intersection Hds1 (af_intersection Hds2t Hds2d))).
    have H: forall s, s = (ds s).1 ++ take 1 (ds s).2 ++ drop 1 (ds s).2
        by move=> s'; rewrite !cat_take_drop.
    move => {Hds1 Hds2t Hds2d} s t [Hds1 [Hds2t [Hds2d |]]];
      first by left; rewrite [s]H [t]H {H}; do !apply: seq_embed_cat => //.
    case Es: _ / snd_dec_splitP => [// | x2 s2 Hs Hx2] => Hd. right.
    rewrite -/ds in Es. rewrite -Es in Hd. rewrite [s]H.
    rewrite -[x0 :: s0]/([::]++[:: x0]++s0).
    do !apply: seq_embed_cat => //.
    by rewrite Es; right.
Qed.

End HigmansLemma.
